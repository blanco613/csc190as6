﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
//needed for SQL access
using System.Data.SqlClient;

namespace TransactionServer
{
    public class Service1 : IStockExchange
    {
        private String TransactionServer = "Data Source=localhost;Initial Catalog=TransactionServer;Integrated Security=True";

        // -- DO NOT touch the getPrice() implementation. WILL BE IMPROVED in revisions later.
        public RESULT_PRICE getPrice(String stock_id)
        {
            RESULT_PRICE result = new RESULT_PRICE();
            result.OK = true;
            result.MESSAGE = "OK";
            result.PRICE_CENTS = 200;
            result.TIMESTAMP = DateTime.Now;
            return result;
        }

        //Task1 (User Story1): Register Account. Check Requirements in IService1.cs
        public RESULT_ACCOUNT RegisterAccount(String uname, String fname, String lname, String password, long initialBalance_100)
        {
            RESULT_ACCOUNT res = new RESULT_ACCOUNT();
            //1. try to establish connection, and begin the transaction
            SqlConnection conn = new SqlConnection(TransactionServer);
            conn.Open();
            SqlTransaction transaction = conn.BeginTransaction(System.Data.IsolationLevel.Serializable);
            //2. perform SQL selection, check existence of UNAME
            try
            {

                String str1 = "SELECT UNAME FROM TBL_ACCOUNT WHERE UNAME=@user";
                SqlCommand cmd1 = new SqlCommand(str1, conn, transaction);
                cmd1.Parameters.AddWithValue("@user", uname);
                SqlDataReader reader1 = cmd1.ExecuteReader();
                if (reader1.HasRows)
                {
                    reader1.Close();
                    throw new Exception("User already exists");
                }
                else                
                //3. perform insertion
                {
                    reader1.Close();
                    String str2 = "INSERT INTO TBL_ACCOUNT(FNAME, LNAME, BALANCE_CENTS, UNAME, PASSWORD) VALUES(@first, @last, @bal, @user, @pwd)";
                    SqlCommand cmd2 = new SqlCommand(str2, conn, transaction);
                    cmd2.Parameters.AddWithValue("@first", fname);
                    cmd2.Parameters.AddWithValue("@last", lname);
                    cmd2.Parameters.AddWithValue("bal", initialBalance_100);
                    cmd2.Parameters.AddWithValue("@user", uname);
                    cmd2.Parameters.AddWithValue("@pwd", password);
                    cmd2.ExecuteNonQuery();
                    res.MESSAGE = "Account created!";
                    transaction.Commit();

                    //4. read out the new account id
                    String str3 = "SELECT ID FROM TBL_ACCOUNT WHERE UNAME=@user";
                    SqlCommand cmd3 = new SqlCommand(str3, conn, transaction);
                    cmd3.Parameters.AddWithValue("@user", uname);
                    SqlDataReader reader2 = cmd3.ExecuteReader();
                    if (reader2.Read())
                    {
                        res.ACCOUNT_ID = reader2.GetInt32(0);
                    }
                    reader2.Close();
                }
            }                
            catch (Exception exc)
            {
            res.OK = false;
            res.MESSAGE = exc.ToString();
            }
            finally
            {
            conn.Close();
            }          
            return res;
        }



        //Task2 (User Story2): Buy. Check Requirements in IService1.cs
        public RESULT_PRICE buy(int account_id, String password, String stock_id, int amount)
        {
            RESULT_PRICE res = new RESULT_PRICE();
            //1. try to establish connection, and begin the transaction
            SqlConnection conn = new SqlConnection(this.TransactionServer);
            conn.Open();
            SqlTransaction trans = conn.BeginTransaction(System.Data.IsolationLevel.Serializable);
            int fee_cents = 200;
            try
            {

                //2. check validity of password, and retrieve the balance of the account
                String strl = "SELECT BALANCE_CENTS FROM TBL ACCOUNT WHERE ID=@IDENTIFIER AND PASSWRD @=PWD";
                SqlCommand cmdl = new SqlCommand(strl, conn, trans);
                cmdl.Parameters.AddWithValue("@identifier", account_id);
                cmdl.Parameters.AddWithValue("@pwd", password);
                SqlDataReader reader1 = cmdl.ExecuteReader();
                if (!reader1.HasRows)
                {
                    reader1.Close();    //close manually to save resources
                    throw new Exception("password not right");
                }
                reader1.Read(); //read out the row
                Int64 balance_cents = reader1.GetInt64(0);    //reposition at 0   

                //3. get the price of the stock, call getPrice(stock_id)
                RESULT_PRICE price = this.getPrice(stock_id);
                String price_as_cents = price.MESSAGE;
                // int price_int = this.getPrice(stock_id);
                int out_price;
                bool success = int.TryParse(price_as_cents, out out_price);
                if (success != true)
                {    //check if the price is negative or not an int
                    throw new Exception("Market's not returning stock prices right now!");
                }
                Int64 remaining_balance = balance_cents - fee_cents - amount * price.PRICE_CENTS;
                if (remaining_balance < 0)
                {
                    throw new Exception("You don't have sufficient funds!");
                }

                //4. update the balance of the account
                reader1.Close();
                String str4 = "UPDATE TBL_ACCOUNT SET BALANCE_CENTS = @newbal WHERE ID =@id";
                SqlCommand cmd4 = new SqlCommand(str4, conn, trans);
                cmd4.Parameters.AddWithValue("@newbal", remaining_balance);
                cmd4.Parameters.AddWithValue("@id", account_id);
                cmd4.ExecuteNonQuery();

                //5. update the ownership table, two cases: UPDATE and INSERT
                String exist_query = "SELECT * FROM TBL_OWNERSHIP WHERE ACCOUNT_ID=@id AND STOCK_ID=@sid";
                SqlCommand exists_cmd = new SqlCommand(exist_query, conn, trans);
                exists_cmd.Parameters.AddWithValue("@id", account_id);
                exists_cmd.Parameters.AddWithValue("@sid", stock_id);
                SqlDataReader exists_reader = cmdl.ExecuteReader();
                if (!exists_reader.HasRows)
                {
                    exists_reader.Close();    //close manually to save resources
                    //Insert Ownership
                    String insert_query = "INSERT INTO TBL_OWNERSHIP(ACCOUNT_ID, STOCK_ID, AMOUNT) VALUES(@id, @sid, @amount)";
                    SqlCommand insert_command = new SqlCommand(insert_query, conn, trans);
                    insert_command.Parameters.AddWithValue("@id", account_id);
                    insert_command.Parameters.AddWithValue("@sid", stock_id);
                    insert_command.Parameters.AddWithValue("@amount", amount);
                    insert_command.ExecuteNonQuery();
					}
                else
                {
                    String update_query = "UPDATE TBL_OWNERSHIP WHERE ACCOUNT_ID=@id AND STOCK_ID=@sid SET AMOUNT+=@amount";
                    SqlCommand update_command = new SqlCommand(update_query, conn, trans);
                    update_command.Parameters.AddWithValue("@id", account_id);
                    update_command.Parameters.AddWithValue("@sid", stock_id);
                    update_command.Parameters.AddWithValue("@amount", amount);
                    int rows_changed = update_command.ExecuteNonQuery();
                    if (rows_changed != 1)
                    {
                        throw new Exception("Could not update ownership table");
                    }
                }

                //6. insert into  the transactions table
                String insert_query2 = "INSERT INTO TBL_TRANSACTIONS(ACCOUNT_ID, STOCK_ID, AMOUNT) VALUES(@id, @sid, @amount)";
                SqlCommand insert_command2 = new SqlCommand(insert_query2, conn, trans);
                insert_command2.Parameters.AddWithValue("@id", account_id);
                insert_command2.Parameters.AddWithValue("@sid", stock_id);
                insert_command2.Parameters.AddWithValue("@amount", amount);
                insert_command2.ExecuteNonQuery();

                //7. commit transaction and return 
                res.OK = true;
                res.MESSAGE = "Buy transaction completed!";
                res.PRICE_CENTS = price.PRICE_CENTS;
                res.STOCK_ID = stock_id;
                res.TIMESTAMP = price.TIMESTAMP;
                trans.Commit();
            }
            catch (Exception exc)
            {
                res.OK = false;
                res.MESSAGE = exc.ToString();
                res.PRICE_CENTS = -1;
                res.TIMESTAMP = DateTime.Now;
                trans.Rollback();
            }
            conn.Close();
            return res;
        }

        //Task3 (User Story3): Sell. Check Requirements in IService1.cs
        public RESULT_PRICE sell(int account_id, String password, String stock_id, int amount)
        {
            //1. try to establish connection, and begin the transaction
            RESULT_PRICE res = new RESULT_PRICE();
            SqlConnection conn = new SqlConnection(this.connStr);
            conn.Open();
            SqlTransaction trans = conn.BeginTransaction(System.Data.IsolationLevel.Serializable);
            try
            {

                //2. check validity of password, and retrieve the balance of the account           
                String strl = "SELECT BALANCE_CENTS FROM TBL ACCOUNT WHERE ID=@IDENTIFIER AND PASSWRD @=PWD";
                SqlCommand cmdl = new SqlCommand(strl, conn, trans);
                cmdl.Parameters.AddWithValue("@identifier", account_id);
                cmdl.Parameters.AddWithValue("@pwd", password);
                SqlDataReader reader1 = cmdl.ExecuteReader();
                if (!reader1.HasRows)
                {
                    reader1.Close();    //close manually to save resources
                    throw new Exception("password not right");
                }
                reader1.Read(); //read out the row
                Int64 balance_cents = reader1.GetInt64(0);    //reposition at 0

                //3. get the price of the stock, call getPrice(stock_id)
                RESULT_PRICE price = this.getPrice(stock_id);
                int price_int;
                bool success = int.TryParse(price.MESSAGE, out price_int);
                if (price_int < 0 || success == false)
                {    //check if the price is negative or not an int
                    throw new Exception("Market's not returning stock prices right now!");
                }

                //4. check/update the ownership table. If the owner does not have so many to sell, throw exception

                String exist_query = "SELECT AMOUNT FROM TBL_OWNERSHIP WHERE ACCOUNT_ID=@id AND STOCK_ID=@sid";
                SqlCommand exists_cmd = new SqlCommand(exist_query, conn, trans);
                exists_cmd.Parameters.AddWithValue("@id", account_id);
                exists_cmd.Parameters.AddWithValue("@sid", stock_id);

                SqlDataReader exists_reader = exists_cmd.ExecuteReader();
                if (!exists_reader.HasRows || exists_reader.GetSqlInt64(amount) < amount)
                {
                    throw new Exception("Not enough to sell!");
                }
                else
                {
                    exists_reader.Close();    //close manually to save resources
                    //Insert Ownership
                    String insert_query = "INSERT INTO TBL_OWNERSHIP(ACCOUNT_ID, STOCK_ID, AMOUNT) VALUES(@id, @sid, @amount)";
                    SqlCommand insert_command = new SqlCommand(insert_query, conn, trans);
                    insert_command.Parameters.AddWithValue("@id", account_id);
                    insert_command.Parameters.AddWithValue("@sid", stock_id);
                    insert_command.Parameters.AddWithValue("@amount", amount);
                    insert_command.ExecuteNonQuery();
                }

                //5. update the balance of the account
                reader1.Close();
                Int64 remaining_balance = balance_cents - 200 + amount * price_int;

                String str6 = "UPDATE TBL_ACCOUNT SET BALANCE_CENTS = @newbal WHERE ID =@id";
                SqlCommand cmd6 = new SqlCommand(str6, conn, trans);
                cmd6.Parameters.AddWithValue("@newbal", remaining_balance);
                cmd6.Parameters.AddWithValue("@id", account_id);
                cmd6.ExecuteNonQuery();

                //6. insert into  the transactions table
                String insert_query2 = "INSERT INTO TBL_TRANSACTIONS(ACCOUNT_ID, STOCK_ID, AMOUNT) VALUES(@id, @sid, @amount)";
                SqlCommand insert_command2 = new SqlCommand(insert_query2, conn, trans);
                insert_command2.Parameters.AddWithValue("@id", account_id);
                insert_command2.Parameters.AddWithValue("@sid", stock_id);
                insert_command2.Parameters.AddWithValue("@amount", amount);
                insert_command2.ExecuteNonQuery();

                //7. commit transaction and return
                res.OK = true;
                res.MESSAGE = "Sell transaction completed!";
                res.PRICE_CENTS = price.PRICE_CENTS;
                res.STOCK_ID = stock_id;
                res.TIMESTAMP = price.TIMESTAMP;
                trans.Commit();
            }
            catch (Exception exc)
            {
                res.OK = false;
                res.MESSAGE = exc.ToString();
                res.PRICE_CENTS = -1;
                res.TIMESTAMP = DateTime.Now;
                trans.Rollback();

            }
            conn.Close();
            return res;
        }
    }

}
