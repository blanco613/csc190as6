﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TransactionServer
{
    /// <summary>
    /// IStockExchange defines the SERVICE CONTRACT of the Exchange Server
    /// </summary>
    [ServiceContract]
    public interface IStockExchange
    {

        /// <summary>
        /// Register a account with the Stock Exchange Server. Deposit and set an initial (i.e.,
        /// amount of cash) of the account
        /// </summary>
        /// <param name="uname">User name of the new account</param>
        /// <param name="fname">First name of the user </param>
        /// <param name="lname">Last name of the user</param>
        /// <param name="password">Password used for identity verification of other operations such as buy/sell</param>
        /// <param name="intialBalance_100">Initial cash deposit in CENTS. E.g., to deposit 100 dollars,
        ///         you need to pass 10000</param>
        /// <returns>If successful, OK attribute will be set to true, and the ACCOUNT_ID sets to the new ID of the account;
        /// if failed, the MESSAGE attribute contains the detailed error message</returns>
        [OperationContract]
        RESULT_ACCOUNT RegisterAccount(String uname, String fname, String lname, String password, long intialBalance_100);

        /// <summary>
        /// Get the price of the given stock ID.
        /// *** THIS IMPLEMENTATION IS TEMPORARY. It returns a flat price 200 (2 dollars) for any stock. Datetime is
        /// set to the current date time.
        /// </summary>
        /// <param name="stock_id">stock id (such as "INTL")</param>
        /// <returns>instance of RESULT_PRICE. Failure causes can include: stock_id not exists, or market closed etc.</returns>
        [OperationContract]
        RESULT_PRICE getPrice(String stock_id);

        /// <summary>
        /// Purchase stock_id for account_id. This operation needs the password of the account_id. If successful,
        /// the operation updates the BALANCE_CENTS of the account table, it updates or inserts new records into
        /// ownership table (depending on if the user has already owned the stock). It also inserts a record to the
        /// transaction table to record this transaction.
        /// 
        /// The operation charges a FLAT TRANSACTION FEE of 2 dollars (200 cents)
        /// </summary>
        /// <param name="account_id">The account that wishes to buy.</param>
        /// <param name="password">The password of the account</param>
        /// <param name="stock_id">The stock to buy</param>
        /// <param name="amount">The quantity to buy.</param>
        /// <returns>If the operation is successful, the RESULT_PRICE contains the purchrase_price (and the transaction
        /// timestamp). If failure, the MESSAGE attribute contains the error message. Failure of a purchase operation
        /// can be caused by: (1) incorrect password, (2) market closed - when getPrice() does not return a valid price, 
        /// (3) the account does not have sufficient fund. </returns>
        [OperationContract]
        RESULT_PRICE buy(int account_id, String password, String stock_id, int amount);

        /// <summary>
        /// Sell stock_id for account_id. This operation needs the password of the account_id. If successful,
        /// the operation updates the BALANCE_CENTS of the account table, it updates or deletes  records in the
        /// ownership table (depending on if the user now own any stock of "stock_id"). It also inserts a record to the
        /// transaction table to record this transaction.
        /// 
        /// The operation charges a FLAT TRANSACTION FEE of 2 dollars (200 cents)
        /// </summary>
        /// <param name="account_id">The account that wishes to sell.</param>
        /// <param name="password">The password of the account</param>
        /// <param name="stock_id">The stock to sell</param>
        /// <param name="amount">The quantity to sell.</param>
        /// <returns>If the operation is successful, the RESULT_PRICE contains the sell_price (and the transaction
        /// timestamp). If failure, the MESSAGE attribute contains the error message. Failure in the sell operation
        /// can be caused by: (1) incorrect password, (2) market closed - when getPrice() does not return a valid price, 
        /// (3) the account does not have the quantity of the stock to sell. </returns>
        [OperationContract]
        RESULT_PRICE sell(int account_id, String password, String stock_id, int amount);
        
    }


    /// <summary>
    /// Entails information of the result of registration/log-in. 
    /// Two cases:
    /// (1) success: OK is true, ACCOUNT_ID has a valid account number
    /// (2) failure: OK is false. MESSAGE contains the detailed error message. ACCOUNT_ID is set to -1.
    /// </summary>
    [DataContract]
    public class RESULT_ACCOUNT
    {
        //******** public data below ******************
        [DataMember]
        public bool OK
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string MESSAGE
        {
            get { return stringValue; }
            set { stringValue = value; }
        }

        [DataMember]
        public int ACCOUNT_ID
        {
            get { return account_id; }
            set { account_id = value; }
        }


                     
        // ******** PRIVATE DATE BELOW ****************
        bool boolValue = true;
        string stringValue = "N/A";
        int account_id = -1;
    }


    /// <summary>
    /// PRICE_RESULT represents a price quote. OK/MESSAGE contains the query results (success or failure)
    /// STOCK_ID: ID of the stock (e.g., INTL)
    /// PRICE_CENTS: price in cents (must be an integer)
    /// TIMESTAMP: the timestamp of the price quote
    /// </summary>
    [DataContract]
    public class RESULT_PRICE
    {
        //******** public data below ******************
        [DataMember]
        public bool OK
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string MESSAGE
        {
            get { return stringValue; }
            set { stringValue = value; }
        }

        [DataMember]
        public string STOCK_ID
        {
            get { return stock_id; }
            set { stock_id = value; }
        }

        [DataMember]
        public int  PRICE_CENTS
        {
            get { return price_cents; }
            set { price_cents = value; }
        }

        [DataMember]
        public DateTime TIMESTAMP
        {
            get { return time_stamp; }
            set { time_stamp = value; }
        }


        // ******** PRIVATE DATE BELOW ****************
        bool boolValue = true;
        string stringValue = "N/A";
        string stock_id = "UNKNOWN";
        int price_cents = 0;
        DateTime time_stamp;
    }

    /// <summary>
    /// RESULT has two attributes: OK (means if the operation is successful), MESSAGE (contains error message)
    /// </summary>
    [DataContract]
    public class ACCOUNT
    {
        //******** public data below ******************
        [DataMember]
        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public String FNAME
        {
            get { return fname; }
            set { fname = value; }
        }

        [DataMember]
        public String LNAME
        {
            get { return lname; }
            set { lname = value; }
        }

        [DataMember]
        public String UNAME
        {
            get { return uname; }
            set { uname = value; }
        }

        [DataMember]
        public String PASSWORD
        {
            get { return password; }
            set { password = value; }
        }

        [DataMember]
        public long BALANCE_100
        {
            get { return balance_100; }
            set { balance_100 = value; }
        }



        // ******** PRIVATE DATE BELOW ****************
        int id;
        String fname;
        String lname;
        String uname;
        String password;
        long balance_100; //the actual balance*100;
        
    }
}
